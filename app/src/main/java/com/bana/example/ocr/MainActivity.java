package com.bana.example.ocr;
/**
 * Created by elban on 10/31/2017.
 */

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends Activity implements View.OnClickListener {

    private static final int RC_OCR_CAPTURE = 9003;
    private static final String TAG = "MainActivity";
    // Use a compound button so either checkbox or switch widgets work.
    private CompoundButton autoFocus;
    private CompoundButton useFlash;
    private TextView statusMessage;
    private TextView textValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusMessage = findViewById(R.id.status_message);
        textValue = findViewById(R.id.text_value);

        autoFocus = findViewById(R.id.auto_focus);
        useFlash = findViewById(R.id.use_flash);

        findViewById(R.id.read_text).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.read_text) {
            // launch Ocr capture activity.
            Intent intent = new Intent(this, OcrCaptureActivity.class);
            intent.putExtra(OcrCaptureActivity.AutoFocus, autoFocus.isChecked());
            intent.putExtra(OcrCaptureActivity.UseFlash, useFlash.isChecked());

            startActivityForResult(intent, RC_OCR_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_OCR_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    String text = data.getStringExtra(OcrCaptureActivity.TextBlockObject);
                    statusMessage.setText(R.string.ocr_success);
                    String address = "", phone = "", mobile = "", email = "";
                    int addressEnd = 0, phoneEnd = 0, mobileEnd = 0, emailEnd = 0;
                    ArrayList<Integer> arrayListStart = new ArrayList<>();
                    if (text.contains("address")) {
                        arrayListStart.add(text.indexOf("address"));
                        addressEnd = text.indexOf("address") + 8;
                    }
                    if (text.contains("phone")) {
                        arrayListStart.add(text.indexOf("phone"));
                        phoneEnd = text.indexOf("phone") + 6;
                    }
                    if (text.contains("mobile")) {
                        arrayListStart.add(text.indexOf("mobile"));
                        mobileEnd = text.indexOf("mobile") + 7;
                    }
                    if (text.contains("email")) {
                        arrayListStart.add(text.indexOf("email"));
                        emailEnd = text.indexOf("email") + 6;
                    }
                    arrayListStart.add(text.length());

                    for (int i = 0; i < arrayListStart.size(); i++) {
                        if (addressEnd < arrayListStart.get(i)) {
                            if ((address.length() > text.substring(addressEnd, arrayListStart.get(i)).length()
                                    && text.contains("address")) || (address.equals("") && text.contains("address"))) {
                                address = text.substring(addressEnd, arrayListStart.get(i));
                                Log.e("address", address);
                            }
                        }
                        if (phoneEnd < arrayListStart.get(i)) {
                            if ((phone.length() > text.substring(phoneEnd, arrayListStart.get(i)).length()
                                    && text.contains("phone")) || (phone.equals("") && text.contains("phone"))) {
                                phone = text.substring(phoneEnd, arrayListStart.get(i));
                                Log.e("phone", phone);
                            }
                        }
                        if (mobileEnd < arrayListStart.get(i)) {
                            if ((mobile.length() > text.substring(mobileEnd, arrayListStart.get(i)).length()
                                    && text.contains("mobile")) || (mobile.equals("") && text.contains("mobile"))) {
                                mobile = text.substring(mobileEnd, arrayListStart.get(i));
                                Log.e("mobile", mobile);
                            }
                        }
                        if (emailEnd < arrayListStart.get(i)) {
                            if ((email.length() > text.substring(emailEnd, arrayListStart.get(i)).length()
                                    && text.contains("email")) || (email.equals("") && text.contains("email"))) {
                                email = text.substring(emailEnd, arrayListStart.get(i));
                                Log.e("email", email);
                            }
                        }
                    }
                    Log.d(TAG, "Text read: " + text);
                    textValue.setText("The Address is : " + address + "\nThe Phone is : " + phone + "\nThe Mobile is : " + mobile + "\nThe Email is : " + email);
                } else {
                    statusMessage.setText(R.string.ocr_failure);
                    Log.d(TAG, "No Text captured, intent data is null");
                }
            } else {
                statusMessage.setText(String.format(getString(R.string.ocr_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
